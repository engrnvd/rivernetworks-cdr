<?php

namespace App\Http\Controllers;

use App\Widget;
use Illuminate\Http\Request;
use App\Http\Requests;

class WidgetController extends Controller
{
    public function index(Request $request)
    {
        if ($request->wantsJson()) {
            return Widget::enabled();
        }
    }

}
