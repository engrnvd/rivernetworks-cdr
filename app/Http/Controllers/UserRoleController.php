<?php
namespace App\Http\Controllers;

use App\User;
use App\UserRole;
use Illuminate\Http\Request;

class UserRoleController extends Controller
{
    public $viewDir = "user_role";

    public function index(Request $request)
    {
        if ($request->wantsJson()) {
            return UserRole::with(UserRole::relations())->get();
        }
        return $this->view("index");
    }

    public function load()
    {
        return UserRole::findRequested();
    }

    public function saveRole(Request $request)
    {
        if (!$request->get('role'))
            abort(403, "Invalid data. No role specified.");
        $role = UserRole::find($request->get('role'));
        /* @var $role UserRole */
        if (!$role)
            abort(404, "User Role not found.");

        if ($request->has('pages')) {
            $attachedList = collect([]);
            foreach ($request->get('pages') as $page) {
                $checked = collect($page['children'])
                    ->filter(function ($node) {
                        return $node['checked'];
                    })->pluck('id')->all();
                $attachedList = $attachedList->merge($checked);
            }
            $role->pageActions()->sync($attachedList->all());
            return $role->pageActions;
        } else if ($request->has('widgets')) {
            $attachedList = collect($request->get('widgets'))->where('checked', true)->pluck('id');
            $role->widgets()->sync($attachedList->all());
            return $role->widgets;
        }
    }

    public function store(Request $request)
    {
        $this->validate($request, UserRole::validationRules());
        return UserRole::create($request->all());
    }

    public function update(Request $request, UserRole $userRole)
    {
        if( $request->wantsJson() )
        {
            $data = [$request->name  => $request->value];
            $validator = \Validator::make( $data, UserRole::validationRules( $request->name ) );
            if($validator->fails())
                return response($validator->errors()->first( $request->name),403);
            $userRole->update($data);
            return "UserRole updated.";
        }

        $this->validate($request, UserRole::validationRules());
        $userRole->update($request->all());
        return redirect('/user-role');
    }

    public function destroy(Request $request, UserRole $userRole)
    {
        if ($userRole->id == 1) {
            abort(400, "Deleting role \"{$userRole->role}\" is not allowed.");
        }
        if ($request->has('newRole')) {
            $newRole = UserRole::find($request->get('newRole'));
            if (!$newRole) {
                abort(400, "The role you are trying to update is not exists");
            }
            User::whereUserRoleId($userRole->id)->update(['user_role_id' => $request->get('newRole')]);
        }
        if (User::whereUserRoleId($userRole->id)->count()) {
            abort(400, "One or more users belong to this role.");
        }
        //Clean relational data before deleting role
        foreach (UserRole::relations() as $relation) {
            $userRole->$relation()->sync([]);
        }
        $userRole->delete();
        return "User Role deleted";
    }

    public function bulkDelete(Request $request)
    {
        $items = $request->items;
        if(!$items) {
            abort(403, "Please select some items.");
        }

        if(!$ids = collect($items)->pluck('id')->all()) {
            abort(403, "No ids provided.");
        }

        UserRole::whereIn('id', $ids)->delete();
        return response("Deleted");
    }

    public function bulkEdit(Request $request)
    {
        if (!$field = $request->field) {
            abort(403, "Invalid request. Please provide a field.");
        }

        if (!$fieldName = array_get($field, 'name')) {
            abort(403, "Invalid request. Please provide a field name.");
        }

        if(!$items = $request->items) {
            abort(403, "Please select some items.");
        }

        if(!$ids = collect($items)->pluck('id')->all()) {
            abort(403, "No ids provided.");
        }

        UserRole::whereIn('id', $ids)->update([$fieldName => array_get($field, 'value')]);
        return response("Updated");
    }

    protected function view($view, $data = [])
    {
        return view($this->viewDir.".".$view, $data);
    }

    public function create()
    {
        abort(404);
    }

    public function show(Request $request, UserRole $userRole)
    {
        abort(404);
    }

    public function edit(Request $request, UserRole $userRole)
    {
        abort(404);
    }

}
