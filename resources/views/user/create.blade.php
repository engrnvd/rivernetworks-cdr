<div class="row">
    <div class="col-md-6 col-sm-12">
        <uib-accordion>
            <uib-accordion-group panel-class="panel-default panel-flat">
                <uib-accordion-heading>
                    <i class="fa fa-plus"></i> Add a New User
                </uib-accordion-heading>
                <nvd-form model="form" on-success="loadUsers()" action="/user">
                    <nvd-form-element field="name">
                        <input class="form-control" ng-model="form.name" placeholder="Name"/>
                    </nvd-form-element>

                    <nvd-form-element field="email">
                        <input class="form-control" ng-model="form.email" placeholder="Email"/>
                    </nvd-form-element>

                    <nvd-form-element field="password">
                        <input type="password" class="form-control" ng-model="form.password" placeholder="Password"/>
                    </nvd-form-element>

                    <nvd-form-element field="status">
                        <select ng-options="option.value as option.label for option in userStatus" class="form-control"
                                ng-model="form.status">
                            <option value="">Status</option>
                        </select>
                    </nvd-form-element>

                    <nvd-form-element field="user_role_id">
                        <select ng-options="option.id as option.role for option in filterService.userRoles"
                                class="form-control"
                                ng-model="form.user_role_id">
                        </select>
                    </nvd-form-element>

                    <button type="submit" class="btn btn-primary btn-flat">Create</button>
                </nvd-form>
            </uib-accordion-group>
        </uib-accordion>
    </div>
</div>