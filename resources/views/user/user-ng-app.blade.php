@push('head-scripts')
    @include('common.services.filter-service')
    <script>
        (function () {
            angular.module("myApp").controller('MainController', MainController);

            function MainController(
                $http,
                $scope,
                filterService,
                PageState,
                toaster
            ) {
                $scope.filterService = filterService;
                $scope.users = [];
                $scope.userRoles = [];
                $scope.recordsInfo = {};
                $scope.userStatus = [
                    {label: 'Enabled', value: 'Enabled'},
                    {label: 'Disabled', value: 'Disabled'},
                ];
                var state = $scope.state = PageState;
                state.loadingUsers = false;
                let form = $scope.form = {
                    status: 'Enabled',
                };

                $scope.loadUsers = function () {
                    $scope.users = [];
                    state.loadingUsers = true;
                    $http.get("/user", {params: state.params}).then(function (response) {
                        $scope.users = response.data.data;
                        $scope.recordsInfo = response.data;
                    }).catch(function (res) {
                        toaster.pop('error', 'Error while loading Users', res.data);
                    }).then(function () {
                        state.loadingUsers = false;
                    });
                };

                $scope.$watch('state.params', $scope.loadUsers, true);

                filterService.loadUserRoles().then(function () {
                    if (filterService.userRoles.length) {
                        form.user_role_id = filterService.userRoles[0].id;
                    }
                });

                $scope.bulkAssignerFields = {
                    status: {name: 'status', label: 'Status'},
                    user_role_id: {name: 'user_role_id', label: 'User Role'}
                };

                $scope.csvFields = [
                    {name: 'id', label: 'Id'},
                    {name: 'name', label: 'Name'},
                    {name: 'email', label: 'Email'},
                    {name: 'status', label: 'Status'},
                    {name: 'user_role_id', label: 'User Role Id'},
                    {name: 'created_at', label: 'Created At'},
                    {name: 'updated_at', label: 'Updated At'}
                ];
            }
        })();
    </script>
@endpush