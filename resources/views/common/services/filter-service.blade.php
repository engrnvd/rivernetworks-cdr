<script>
    (function () {
        'use strict';
        angular.module("myApp").factory('filterService', filterService);

        function filterService($http, toaster) {
            let loading = {
                userRoles: false,
            };
            let service = {
                loading: loading,
                userRoles: [],
            };

            service.load = function (url, key) {
                if (!url.startsWith("/")) url = "/" + url;
                loading[key] = true;
                return $http.get(url).then(function (response) {
                    service[key] = response.data;
                    loading[key] = false;
                }).catch(function (response) {
                    loading[key] = false;
                    toaster.pop('error', 'Error while loading ' + key + '. Try refresh.', response.data);
                });
            };

            service.loadUserRoles = function () {
                return service.load('load-user-roles', 'userRoles');
            };

            return service;
        }
    })();
</script>