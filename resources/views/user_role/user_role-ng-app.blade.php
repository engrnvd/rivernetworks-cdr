@push('head-scripts')
    <script>
        (function () {
            angular.module("myApp").controller('MainController', MainController);

            function MainController(
                $http,
                $scope,
                $uibModal,
                NvdNgTreeService,
                toaster
            ) {
                $scope.form = {};
                $scope.roles = [];
                $scope.pages = [];
                $scope.widgets = [];
                $scope.tree = {
                    pages: new NvdNgTreeService([]),
                    widgets: new NvdNgTreeService([]),
                };
                var state = $scope.state = {
                    loading: {
                        roles: false,
                        pages: false,
                        widgets: false,
                        saveResponse: false
                    },
                    selectedRole: null
                };

                $scope.loadRoles = function () {
                    if (state.loading.roles) return;
                    state.loading.roles = true;
                    return $http.get("/user-role").then(function (response) {
                        $scope.roles = response.data;
                        if ($scope.roles.length && !state.selectedRole)
                            state.selectedRole = $scope.roles[0];
                        else
                            state.selectedRole = $scope.roles.findOne(function (role) {
                                return role.id == state.selectedRole.id;
                            });
                    }).catch(function (response) {
                        toaster.pop('error', 'Error while loading user roles. Try refresh.', response.data);
                    }).then(function () {
                        state.loading.roles = false;
                    });
                };

                $scope.loadPages = function () {
                    if (state.loading.pages) return;
                    state.loading.pages = true;
                    return $http.get("/page").then(function (response) {
                        $scope.pages = response.data;
                        $scope.makePagesTree();
                    }).catch(function (response) {
                        toaster.pop('error', 'Error while loading pages.', response.data);
                    }).then(function () {
                        state.loading.pages = false;
                    });
                };

                $scope.makePagesTree = function () {
                    var pages = $scope.pages;
                    var rootNode = {
                        id: -1,
                        label: "All Pages",
                        children: [],
                        opened: true
                    };

                    for (var $i = 0; $i < pages.length; $i++) {
                        var currentPage = pages[$i];
                        var pageNode = {id: -1 * ($i + 2), label: currentPage.title, children: [], opened: false};
                        for (var $j = 0; $j < currentPage.actions.length; $j++) {
                            var currentAction = currentPage.actions[$j];
                            var actionNode = {id: currentAction.id, label: currentAction.title};
                            var actionInSelectedRole = state.selectedRole.page_actions.findOne(function (role) {
                                return role.id == actionNode.id;
                            });
                            if (actionInSelectedRole)
                                actionNode.checked = true;
                            pageNode.children.push(actionNode)
                        }
                        rootNode.children.push(pageNode);
                    }

                    $scope.tree.pages = new NvdNgTreeService([rootNode]);
                };
                $scope.loadWidgets = function () {
                    if (state.loading.widgets) return;
                    state.loading.widgets = true;
                    return $http.get("/widget").then(function (response) {
                        $scope.widgets = response.data;
                        $scope.makeWidgetsTree();
                    }).catch(function (response) {
                        toaster.pop('error', 'Error while loading widgets. Try refresh.', response.data);
                    }).then(function () {
                        state.loading.widgets = false;
                    });
                };
                $scope.makeWidgetsTree = function () {
                    var rootNode = {id: -1, label: "All Widgets", children: [], opened: true};
                    $scope.widgets.forEach(function (widget) {
                        var childNode = {id: widget.id, label: widget.title, opened: true};
                        var widgetInSelectedRole = state.selectedRole.widgets.findOne(function (role) {
                            return role.id == widget.id;
                        });
                        if (widgetInSelectedRole)
                            childNode.checked = true;

                        rootNode.children.push(childNode);
                    });
                    $scope.tree.widgets = new NvdNgTreeService([rootNode]);
                };

                $scope.showLoader = function () {
                    for (let module in state.loading) {
                        if (state.loading[module]) return true;
                    }
                    return false;
                };

                $scope.saveRole = function (option) {
                    var data = {
                        role: state.selectedRole.id,
                    };
                    var checked = $scope.tree[option].getChecked();
                    data[option] = checked.length ? checked[0].children : [];
                    state.loading.saveResponse = true;
                    return $http.post('/user-role/save-role', data).then(function (res) {
                        state.selectedRole[option] = res.data;
                        toaster.pop('success', '', 'Saved');
                    }).catch(function (res) {
                        toaster.pop('error', 'Error while saving settings', res.data.hasOwnProperty('message') ? res.data.message : res.data);
                    }).then(function () {
                        state.loading.saveResponse = false;
                    });
                };

                $scope.deleteRole = function (newRole) {
                    if (!newRole) {
                        if (!confirm('Are you sure want to delete role?')) {
                            return;
                        }
                    }
                    if (state.loading.roles) return;
                    state.loading.roles = true;
                    var data = {_method: "DELETE"};
                    if (newRole) data.newRole = newRole;
                    $http.post("/user-role/" + state.selectedRole.id, data).then(function (response) {
                        $scope.roles.remove(state.selectedRole);
                        state.selectedRole = $scope.roles[0];
                    }).catch(function (response) {
                        if (response.data == 'One or more users belong to this role.') {
                            $scope.reassignRole(response.data);
                            return;
                        }
                        toaster.pop('error', 'Error while deleting role.', response.data.hasOwnProperty('message') ? response.data.message : response.data);
                    }).then(function () {
                        state.loading.roles = false;
                    });
                };
                $scope.reassignRole = function (message) {
                    var roles = $scope.roles;
                    var roleToDelete = state.selectedRole;
                    var modalInstance = $uibModal.open({
                        animation: true,
                        templateUrl: 'assign-new-role.html',
                        controller: function ($scope, $uibModalInstance) {
                            $scope.message = message;
                            $scope.roles = roles;
                            $scope.roleToDelete = roleToDelete;
                            $scope.newRole = null;
                            for (var i = 0; i < roles.length; i++) {
                                if (roles[i].id != roleToDelete.id) {
                                    $scope.newRole = roles[i].id;
                                    break;
                                }
                            }
                            $scope.ok = function () {
                                $uibModalInstance.close($scope.newRole);
                            };
                            $scope.cancel = function () {
                                $uibModalInstance.dismiss('cancel');
                            };
                        }
                    });
                    modalInstance.result.then(function (newRole) {
                        $scope.deleteRole(newRole);
                    });
                };

                $scope.loadRoles().then(function () {
                    $scope.loadPages();
                    $scope.loadWidgets();
                });

                $scope.$watch('state.selectedRole', function () {
                    $scope.makePagesTree();
                    $scope.makeWidgetsTree();
                });

                $scope.saveSetting = function () {
                    switch ($scope.roleActiveTab) {
                        case 0:
                            $scope.saveRole('pages');
                            break;
                        case 1:
                            $scope.saveRole('widgets');
                            break;
                        default:
                            return;
                    }
                };

                $scope.csvFields = [
                    {name: 'id', label: 'Id'},
                    {name: 'role', label: 'Role'},
                    {name: 'default_read_access', label: 'Default Read Access'},
                    {name: 'default_cud_access', label: 'Default Cud Access'},
                    {name: 'created_at', label: 'Created At'},
                    {name: 'updated_at', label: 'Updated At'}
                ];
            }
        })();
    </script>
    <script type="text/ng-template" id="assign-new-role.html">
        <a href="" ng-click="cancel()" class="close-btn"><i class="fa fa-times"></i></a>
        <div class="modal-header">
            <h3 class="modal-title">Reassign role "@{{ roleToDelete.role }}" admins.</h3>
        </div>
        <div class="modal-body" id="modal-body">
            <div class="alert alert-warning">
                <i class="fa fa-exclamation-triangle"></i> Warning: @{{ message }}
            </div>
            <div class="form-group">
                <label for="newRole">Select new role for admins belonging to "@{{ roleToDelete.role }}"</label>
                <select class="form-control" ng-model="newRole"
                        ng-options="role.id as role.role disable when role.id == roleToDelete.id for role in roles">
                </select>
            </div>
        </div>

        <div class="modal-footer">
            <button class="btn btn-danger" type="button" ng-click="ok()">Update admins and delete role</button>
            <button class="btn btn-warning" type="button" ng-click="cancel()">Cancel</button>
        </div>
    </script>
@endpush