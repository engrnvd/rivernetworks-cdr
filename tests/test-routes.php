<?php
Route::any('/test-redis', function () {
    $redis = app('redis');
    // show form to delete a key
    echo '<form method="post"><input name="key-to-del"><button>Delete</button></form>';
    if($keyToDel = request('key-to-del')) {
        $redis->del($keyToDel);
        return redirect('/test-redis');
    }

    $query = "*";
    if (request('keys')) {
        $query = request('keys');
    }
    $keys = $redis->keys($query);
    foreach ($keys as $key) {
        if ($redis->type($key) == 'hash') {
            pr($redis->hgetall($key), $key);
        } elseif ($redis->type($key) == 'set') {
            pr($redis->smembers($key), $key);
        } elseif ($redis->type($key) == 'zset') {
            pr($redis->zrange($key, 0, 0), $key);
        } elseif ($redis->type($key) == 'list') {
            pr($redis->lrange($key, 0, 0), $key);
        } else {
            echo $key . ": ";
            echo $redis->get($key);
        }
        echo "<br>ttl: " . $redis->ttl($key) . "<br><hr>";
    }
    exit;
});

/*
 * Lists the routes
 * You can use this list to get info about a specif route
 * and use it e.g. in page_actions table
 * */
Route::get('routes', function () {
    $routeCollection = Route::getRoutes();
    echo "<table style='width:100%'>";
    echo "<tr>";
    echo "<th width='6%'>Method</th>";
    echo "<th width='24%'>Route</th>";
    echo "<th width='70%'>Corresponding Action</th>";
    echo "</tr>";
    foreach ($routeCollection as $value) {
        /* @var $value Illuminate\Routing\Route*/
        echo "<tr>";
        echo "<td>" . $value->methods()[0] . "</td>";
        echo "<td>" . $value->uri() . "</td>";
        echo "<td>" . $value->getActionName() . "</td>";
        echo "</tr>";
    }
    echo "</table>";
});