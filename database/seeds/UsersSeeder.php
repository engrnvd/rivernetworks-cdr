<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();
        DB::table('users')->insert([
            [
                'name' => 'James Kinnett',
                'email' => 'jameskinnett@rivernetworks.com',
                'password' => bcrypt('123456'),
                'user_role_id' => 1,
                'status' => 'Enabled',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'Jeremy Cornwell',
                'email' => 'jeremycornwell@rivernetworks.com',
                'password' => bcrypt('123456'),
                'user_role_id' => 1,
                'status' => 'Enabled',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'Project Management',
                'email' => 'projectmanagement@rivernetworks.com',
                'password' => bcrypt('123456'),
                'user_role_id' => 1,
                'status' => 'Enabled',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'Naveed',
                'email' => 'naveed@rivernetworks.com',
                'password' => bcrypt('123456'),
                'user_role_id' => 1,
                'status' => 'Enabled',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'Shahid',
                'email' => 'shahid@rivernetworks.com',
                'password' => bcrypt('123456'),
                'user_role_id' => 1,
                'status' => 'Enabled',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
        ]);
    }
}
